# Dialer - project 2
Web app that allows users to making phone calls through browser.

### Prerequisites
* [Node.js (v8.x)](https://nodejs.org/en/)
* [npm (v5.x)](https://www.npmjs.com/)
---

## Installation
Download [backend app](https://bitbucket.org/lucvane/wsk_project1_edited/src/master/)

Before running app, set login and password in app.js file:
```javascript
const config = {
    url: "https://uni-call.fcc-online.pl",
    login: 'PUT LOGIN HERE',
    password: 'PUT PASSWORD HERE'
}
```
Open terminal and go to your project folder using "cd" command. 
For example:
```bash
cd C:\wsk_project1_edited
```
If your project path is correct then run following comand in terminal:
```bash
npm install
node app.js
```
If app is running correctly, the Comand Interpreter shoudl display:
```
app listening on port 3000
``` 
After that

Open another terminal and go to your "WSK_Project_2" project folder using "cd" command. 
For example:
```bash
cd C:\wsk_project_2
cd front
```
If your project path is correct then run following comand in terminal:
```bash
npm install
ng serve
```
If app is running correctly, the Comand Interpreter shoudl display:
```
** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **
``` 
---
## Making calls through browser.
When app is running correctly, go to:
```
http://localhost:4200/
```
Enter correct cell phone number and click button "Zadzwoń".
import { Component, OnInit } from '@angular/core';
import { CallService } from '../call.service';

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.css'],
})
export class WidgetComponent implements OnInit {
  number: string;
  validator = /(^[0-9]{9}$)/;
  state: string = 'waiting';
  interval: number;
  message: string;
  messages: string[] = [];

  constructor(private callService: CallService) {}




  sendMessage() {
    this.callService.sendMessage(this.message);
    this.message = '';
  }
  ngOnInit() {
    this.callService
      .getMessages()
      .subscribe((message: string) => {
        this.messages.push(message);
      });
  }

  call() {
    if (this.isValidNumber()) {
      this.callService.placeCall(this.number);
      this.state = 'ringing';
      this.callService.getCallId().subscribe(() => {
        this.checkStatus();
      });
    } else {
      console.info('Numer niepoprawny');
    }
  }

  isValidNumber(): Boolean {
    return this.validator.test(this.number);
  }

  checkStatus() {
    this.callService.getCallStatus().subscribe(state => {
      this.state = state;
    });
  }
}
